Currently this project has two WIP instances:
- Demo instance - https://ea-cms-template.us.aldryn.io/en/
- Effective Altruism Netherlands - https://effective-altruismnl.us.aldryn.io/en/
- 1fortheworld.org chapter leader resource - https://chapter-leader-resource.us.aldryn.io/en/


Development Setup
-------------------------------------------------------------------------------
Built on Python 3.6, Django 2.2, DjangoCMS 3.7, Webpack 4, TypeScript 3.

See the general [setup instructions](https://gitlab.com/what-digital/djangocms-template/-/blob/master/docs/setup-instruction.md)

[Project intro & guidelines](https://gitlab.com/what-digital/djangocms-template/-/blob/master/docs/README.md)


Codebase Source
-------------------------------------------------------------------------------

This project is built upon [ea-cms-template](https://gitlab.com/effective-altruism/ea-cms-template)
which is built upon [djangocms-template](https://gitlab.com/what-digital/djangocms-template).

The fixes & features from both of those projects are being regularly merged into this repository.
